import re

def hour_to_time(hour):
    """Converts to string and pads the hour with a leading 0 if under 10"""
    if hour < 10:
        return "0" + str(hour)
    return str(hour)

def next_time(time):
    """Given a time of format xx:00/30 AM/PM returns the next one"""
    match = re.match("([0-9]+):([0-9]+) (AM|PM)",time)
    hour, minute, half = (int(match.group(1)),int(match.group(2)),match.group(3))
    if minute == 30:
        hour += 1
        if hour == 12:
            half = "AM" if half == "PM" else "PM" 
        hour = 1 if hour == 13 else hour
        return hour_to_time(hour) + ":00 " + half
    return time.replace(":00",":30") 


class GSR(object):
    """Class for representing a particular GSR and its availability on a given day"""
    def __init__(self,room):
        self.room = room
        self.slots = {}
        for i in range(12):
            base = hour_to_time(i+1) + ":"
            self.slots[base + "00 PM"] = 0
            self.slots[base + "00 AM"] = 0
            self.slots[base + "30 PM"] = 0
            self.slots[base + "30 AM"] = 0

    def fill(self,time):
        """Accepts time of format xx:00/30 AM/PM and fills the availability"""
        self.slots[time] = 1

    def is_available(self,time,duration):
        """
        Accepts time of format xx:00/30 AM/PM
        Duration of format 30/60/90
        """
        avail = True
        for i in range(duration / 30):
            if not avail:
                return False
            avail = self.slots[time] == 0
            time = next_time(time)
        return avail


    def __repr__(self):
        return "GSR " + self.room

    def __cmp__(self,other):
        return cmp(self.room,other.room)

class Huntsman(object):
    """Class representing a collection of GSRs on a particular day"""
    def __init__(self,gsr_list):
        self.gsrs = {}
        for _,gsr in gsr_list.items():
            for _,slot in gsr.items():
                time,room,available = slot
                if room not in self.gsrs:
                    self.gsrs[room] = GSR(room)
                if not available:
                    self.gsrs[room].fill(time)

    def find_gsrs(self,time,duration):
        """
        Accepts time of format xx:00/30 AM/PM
        Returns available gsrs of the exact duration.
        """
        return [gsr.room for _,gsr in self.gsrs.items() if gsr.is_available(time,duration)]

    def find_gsrs_for(self,time,duration):
        """
        Accepts time of format xx:00/30 AM/PM
        Returns available gsrs of the highest duration available up to specified.
        """
        while duration > 0:
            result = self.find_gsrs(time,duration)
            print "Results for duration %d are %s" % (duration,str(result))
            if len(result) > 0:
                return result
            duration -= 30



