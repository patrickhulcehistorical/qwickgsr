import spike
from flask import Flask, request

app = Flask(__name__)

@app.route("/testbook")
def test():
    stuff = []
    stuff.append("<html><body><form action='/book' method='post'>")
    stuff.append("<input type='text' name='username' />")
    stuff.append("<input type='text' name='password' />")
    stuff.append("<input type='text' name='date' />")
    stuff.append("<input type='text' name='time' />")
    stuff.append("<input type='text' name='duration' />")
    stuff.append("<input type='submit' />")
    stuff.append("</form></body></html>")
    return "".join(stuff)

@app.route("/book",methods=['POST'])
def book():
    user = request.form['username']
    date = request.form['date']
    time = request.form['time']
    duration = int(request.form['duration'])
    print date
    #Do the requests
    s = spike.login(user,request.form['password'])
    print "Logged In"
    gsr_list = spike.get_gsrs(s,date)
    print "GSR list is " + str(gsr_list)
    gsrs = gsr_list.find_gsrs(time,duration)
    print gsrs
    if len(gsrs) == 0:
        return "Error\nNo available GSRs at that time."
    if spike.request_gsr(s,date,time,duration,user,gsrs[0]):
        return "Success\nBooked GSR " + gsrs[0]
    return "Error\nCouldn't book GSR %s, something went wrong." % gsrs[0]

if __name__ == "__main__":
    app.debug = True
    app.run()