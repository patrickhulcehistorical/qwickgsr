import requests
import re
from bs4 import BeautifulSoup
from gsr import Huntsman

LOGIN_URL = "https://spike.wharton.upenn.edu/index.cfm?login=true"
GSR_BASE = "https://spike.wharton.upenn.edu/calendar/gsr.cfm"


def login(username,password):
    s = requests.session()
    s.post(LOGIN_URL,data={"username":username,"password":password})
    return s

def load_user_pass():
    with open("creds.conf","r") as f:
        return (str(f.readline()).strip(),str(f.readline().strip()))

def get_gsrs(s,date):
    """Returns a Huntsman collection of GSRs on a given date"""
    r = s.get(GSR_BASE + "?date=" + date)
    print "Got response"
    soup = BeautifulSoup(r.text,"lxml")
    print "Souped it up"
    #Get all the reservation slots
    squares = soup.find_all('a',attrs={"class":"calendarsquare"})
    gsr_list = {}
    for sq in squares:
        parse_square(sq,gsr_list)
    print "Parsed the squares"
    #Get all the current reservations
    blocks = soup.find('div',id="calendar_events").find_all('div')
    for blk in blocks:
        parse_block(blk,gsr_list)
    return Huntsman(gsr_list)

def parse_square(html,gsr_list):
    """Updates the gsr_list with all the room times for each gsr"""
    time, room = html.attrs['title'].split(" - ")
    top = get_px_from_style(html.attrs['style'],"top")
    left = get_px_from_style(html.attrs['style'],"left")
    if left in gsr_list:
        gsr = gsr_list[left]
    else:
        gsr_list[left] = {}
        gsr = gsr_list[left]
    # Set each time slot to available
    gsr[top] = (time,room,True)

def parse_block(html,gsr_list):
    """Updates the gsr_list with unavailable time slots"""
    top = get_px_from_style(html.attrs['style'],"top")
    left = get_px_from_style(html.attrs['style'],"left")
    bot = top + get_px_from_style(html.attrs['style'],"height")
    gsr = None
    try:
        # Try to find the right room by key
        gsr = gsr_list[left]
    except KeyError:
        # Otherwise just look through all of the rooms
        for v in gsr_list:
            if abs(v - left) < 5:
                gsr = gsr_list[v]
                break
    if gsr is None:
        return
    # Look through all of the times and fill in the reservations as unavailable
    for k in gsr:
        if top <= k + 3 and k + 3 < bot:
            gsr[k] = (gsr[k][0],gsr[k][1],False)

def request_gsr(s,date,time,duration,user,room):
    req_data = {}
    req_data['reserved_for'] = user
    req_data['reserved_by'] = user
    req_data['ip_address'] = get_public_ip()
    req_data['GSRQF_date'] = date
    req_data['GSRQF_start_time'] = time
    req_data['GSRQF_attendees'] = user + ","
    req_data['GSRQF_room_number'] = room
    req_data['GSRQF_duration'] = duration
    req_data['GSRQF_title'] = "GSR Reservation"
    req_data['GSRQF_attendees_search'] = ""
    req_data['submit'] = "Submit Request"
    r = s.post(GSR_BASE,data=req_data)
    print "Got booking response"
    soup = BeautifulSoup(r.text,'lxml')
    if soup.find('div',id='calendar_event_detail') is None:
        return False
    return True

def get_px_from_style(style,attr):
    dec = re.search(attr+"[^:]*:([^;]*)px;",style).group(1)
    return int(float(dec))

def test_process():
    s = login(*load_user_pass())
    hunt = get_gsrs(s,"5/16/2013")
    return hunt

def get_public_ip():
    r = requests.get("http://checkip.dyndns.org")
    return re.search(r'\d+\.\d+\.\d+\.\d+', r.text).group()


if __name__ == "__main__":
    print test_process()
    

