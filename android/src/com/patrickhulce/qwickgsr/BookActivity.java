package com.patrickhulce.qwickgsr;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TimePicker;

public class BookActivity extends Activity {
	private Spinner date = null;
	private TimePicker timepicker = null;
	private boolean intervalListen = true;

	private Date properDate(int offset) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_YEAR, offset);
		return c.getTime();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_book);

		List<String> days = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		days.add("Today");
		days.add("Tomorrow");
		days.add(sdf.format(properDate(2)));
		days.add(sdf.format(properDate(3)));
		days.add(sdf.format(properDate(4)));
		days.add(sdf.format(properDate(5)));
		days.add(sdf.format(properDate(6)));

		date = (Spinner) findViewById(R.id.day);

		// Create an ArrayAdapter using the string array and a default spinner
		// layout
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, days);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		date.setAdapter(adapter);

		timepicker = (TimePicker) findViewById(R.id.time);
		timepicker.setCurrentMinute(0);
		TimePicker.OnTimeChangedListener timelistener = new TimePicker.OnTimeChangedListener() {

			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				int nextMinute = 0;
				int nextHour = hourOfDay;
				if (minute == 1) {
					nextMinute = 30;
				}
				if(minute == 59) {
					nextMinute = 30;
					nextHour += 1;
				}
				if (intervalListen) {
					intervalListen = false;
					view.setCurrentMinute(nextMinute);
					view.setCurrentHour(nextHour);
					intervalListen = true;
				}
			}
		};
		timepicker.setOnTimeChangedListener(timelistener);
	}

	public void bookGSR(View v) {
		System.out.println("Clicked!");
		int hour = timepicker.getCurrentHour();
		int minute = timepicker.getCurrentMinute();
		String end = "AM";
		if (hour >= 12) {
			end = "PM";
			hour = hour % 12;
		}
		if (hour == 0) {
			hour = 12;
		}
		String hourTxt = (hour > 9) ? "" + hour : "0" + hour;
		String minuteTxt = (minute >= 30) ? "30" : "00";
		String timeString = hourTxt + ":" + minuteTxt + " " + end;
		int selection = date.getSelectedItemPosition();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_YEAR, selection);
		SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy");
		String dateString = sdf.format(c.getTime());
		System.out.println(timeString);
		System.out.println(dateString);
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		String pennkey = prefs.getString("pennkey", "");
		String password = prefs.getString("password", "");
		System.out.println("PennKey: " + pennkey);
		System.out.println("Password: " + password);
		BookActivity.doServerBookRequest(pennkey, password, dateString, timeString, "90", this);
	}

	public static void doServerBookRequest(final String pennkey, final String password,
			final String date, final String time, final String duration, final Context context) {
		AsyncTask<Void, String, ArrayList<String>> request = new AsyncTask<Void, String, ArrayList<String>>() {

			@Override
			protected ArrayList<String> doInBackground(Void... v) {
				ArrayList<String> lines = new ArrayList<String>();
				HttpURLConnection urlConnection = null;
				try {
					String urlParams = "pennkey=" + URLEncoder.encode(pennkey) +
							"&password=" + URLEncoder.encode(password) +
							"&date=" + URLEncoder.encode(date) +
							"&time=" + URLEncoder.encode(time) +
							"&duration=" + URLEncoder.encode(duration);
					System.out.println(urlParams);
					URL url = new URL("http://www.gsr.hulce.net/book");
					urlConnection = (HttpURLConnection) url.openConnection();
					//Configure POST options
					urlConnection.setRequestMethod("POST");
					urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
					urlConnection.setRequestProperty("Content-Length", "" + urlParams.getBytes().length);
					urlConnection.setUseCaches(false);
					urlConnection.setDoInput(true);
					urlConnection.setDoOutput(true);
					//Send the request
					DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
					wr.writeBytes(urlParams);
					wr.flush();
					wr.close();
					//Read the result
					InputStreamReader isr = new InputStreamReader(
							urlConnection.getInputStream());
					BufferedReader br = new BufferedReader(isr);
					for (String line = br.readLine(); line != null; line = br
							.readLine()) {
						System.out.println(line);
						lines.add(line);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					if (urlConnection != null) {
						urlConnection.disconnect();
					}
				}
				return lines;
			}

			@Override
			protected void onPostExecute(ArrayList<String> result) {
				super.onPostExecute(result);
				System.out.println("Executed");
			}
		};
		request.execute(null,null);
	}

	public void goToSettings(View v) {
		Intent i = new Intent(this, SettingsActivity.class);
		startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_book, menu);
		return true;
	}

}
