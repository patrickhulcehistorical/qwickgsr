package com.patrickhulce.qwickgsr;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;

public class GSRWidget extends AppWidgetProvider {
	public static final String ACTION_BOOK = "GSRWidgetBookAction";

	@Override
	public void onReceive(Context context, Intent i) {
		if (i.getAction().equals(ACTION_BOOK)) {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(context);
			BookActivity.doServerBookRequest(prefs.getString("pennkey", ""),
					prefs.getString("password", ""),
					prefs.getString("widget_date", ""),
					prefs.getString("widget_time", ""),
					prefs.getString("widget_duration", "90"),
					null);
		} else {
			super.onReceive(context, i);
		}
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {

		// Get all ids
		ComponentName thisWidget = new ComponentName(context, GSRWidget.class);
		int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
		for (int widgetId : allWidgetIds) {
			// Create some random data
			RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget);

			// Set the text
			remoteViews.setTextViewText(R.id.datetime, "5/16/2013 8:00 PM");

			// Register an onClickListener
			Intent intent = new Intent(context, GSRWidget.class);

			intent.setAction(ACTION_BOOK);
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);

			PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
					0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.quickbook, pendingIntent);
			appWidgetManager.updateAppWidget(widgetId, remoteViews);
		}
	}

}
